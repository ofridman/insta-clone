//
//  Post.swift
//  instaclone
//
//  Created by Axl on 22/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import Foundation
class Post {
    var user: InstaUser
    var postId: String
    var imageUrl: String
    var caption: String
    var cretaionDate: Date
    init(user:InstaUser,dictionary:[String : Any], postId:String) {
    
        self.user = user
        
        self.postId = postId
        
        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
        
        self.caption = dictionary["caption"] as? String ?? ""
        
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        
        self.cretaionDate = Date(timeIntervalSince1970: secondsFrom1970)
        
    }
}
