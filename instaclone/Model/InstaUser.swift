//
//  InstaUser.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase
class InstaUser {
    var username: String?
    var profileImageUrl: String?
    var uid: String?
    var email: String?
    var following: Int?
    var posts: Int?
    init(uid: String, values:[String:Any] ) {
        self.uid = uid
        self.username = values["username"] as? String ?? ""
        self.profileImageUrl = values["profileImageUrl"] as? String ?? ""
        self.email = values["email"] as? String ?? ""
    }
 
     static func fecthUser(uid: String, completion: @escaping(InstaUser)->  ()) {

        let userRef = Database.database().reference().child("users").child(uid)
        
        userRef.observeSingleEvent(of: .value, with: { (data) in
            
            guard let dic = data.value as? [String:Any] else { return }
            
            let instaUser = InstaUser(uid: uid, values: dic)
        
            completion(instaUser)
            
        }) { (err) in
            print("fechtUser -> ",err)
            
        }
    
    }
    
}
