//
//  LoadImage.swift
//  instaclone
//
//  Created by Axl on 24/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import FirebaseAuth


class Helper{
    
    static let shared = Helper()
    var myView = UIView()
    var myActivityIndicator = UIActivityIndicatorView()
    func loadPhotoImage(url:String,toImageView: UIImageView)  {
        
        let url = URL(string: url)
        URLSession.shared.dataTask(with: url!) { (data, res, err) in
            if let err = err {
                print("Error LoadImage -> loadPhotoImage -> ",err)
                return
            }
            guard let imageData = data else {return}
            
            let photoImage = UIImage(data: imageData)
            
            DispatchQueue.main.async {
                toImageView.image = photoImage
            }
            }.resume()
    }
    func showOKAlert(title:String , message:String, viewController:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
        return
    }
    func showLoading(view:UIView){
        
        // loading
        myView = UIView(frame: CGRect(x: view.frame.midX - 50, y: view.frame.midY - 50, width: 100, height: 100))
        myView.layer.cornerRadius = 15
        myView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        view.addSubview(myView)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        activityIndicator.startAnimating()
        myView.addSubview(activityIndicator)
       
        
    }
    func dateFormatter(timestamp: Double?) -> String? {
        
        if let timestamp = timestamp {
            let date = Date(timeIntervalSinceReferenceDate: timestamp)
            let dateFormatter = DateFormatter()
            let timeSinceDateInSeconds = Date().timeIntervalSince(date)
            let secondInDays: TimeInterval = 24*60*60
            if timeSinceDateInSeconds > 7 * secondInDays {
                dateFormatter.dateFormat = "MM/dd/yy"
            } else if timeSinceDateInSeconds > secondInDays {
                dateFormatter.dateFormat = "EEE"
            } else {
                dateFormatter.dateFormat = "h:mm a"
            }
            return dateFormatter.string(from: date)
        } else {
            return nil
        }
        
    }
}


