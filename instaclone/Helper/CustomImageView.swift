//
//  CustomImageView.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase

var imageCache = [String:UIImage]()


class CustomImageView: UIImageView {
    
    var lastUrlUsedToLoadImage : String?
    
    func loadImage(url:String){
        
        lastUrlUsedToLoadImage = url
        
        if let image = imageCache[url] {
            self.image = image
            return
        }
        
        guard let url = URL(string: url) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, res, err) in
            if let err = err {
                print("Error CustomImageView -> loadImage -> ",err)
                return
            }
            if url.absoluteString != self.lastUrlUsedToLoadImage {
                
                return
                
            }
            guard let imageData = data else {return}
            
            let photoImage = UIImage(data: imageData)
            
            imageCache[url.absoluteString] = photoImage
            
            DispatchQueue.main.async {
                self.image = photoImage
            }
            
            
        }.resume()
        
    }
}
