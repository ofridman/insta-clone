//
//  TestCode.swift
//  instaclone
//
//  Created by Axl on 25/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import SnapKit


class TestCode: UIViewController {
    let profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 40
        iv.backgroundColor =  UIColor(white: 0.9, alpha: 0.8)
        return iv
    }()
    let usernameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "username"
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        
        return lbl
    }()
    let lblFollowing: UILabel = {
        let lbl = UILabel()
        lbl.text = "following"
        lbl.font = UIFont.systemFont(ofSize: 14)
        
        return lbl
    }()
    let lblFollowingCount: UILabel = {
        let lbl = UILabel()
        lbl.text = "11"
        lbl.font = UIFont.systemFont(ofSize: 14)
        
        return lbl
    }()
    let lblFollower: UILabel = {
        let lbl = UILabel()
        lbl.text = "follower"
        lbl.font = UIFont.systemFont(ofSize: 14)
        
        return lbl
    }()
    let lblFollowerCount: UILabel = {
        let lbl = UILabel()
        lbl.text = "11"
        lbl.font = UIFont.systemFont(ofSize: 14)
        return lbl
    }()
    let lblPost: UILabel = {
        let lbl = UILabel()
        lbl.text = "posts"
        lbl.font = UIFont.systemFont(ofSize: 14)
        return lbl
    }()
    let lblPostCount: UILabel = {
        let lbl = UILabel()
        lbl.text = "11"
        lbl.font = UIFont.systemFont(ofSize: 14)
        return lbl
    }()
    let optionsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("***", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        return btn
    }()
    let LikeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let commentButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "comment").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let sendMessageButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "send2").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let bookmarkButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "ribbon").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let captionLabel: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        return lbl
    }()
    let view1: UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    let btnTest: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Edit profile", for: .normal)
        btn.tintColor = UIColor.black
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth  = 1
        btn.layer.cornerRadius = 3
        return btn
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    func setupView(){
        usernameLabel.text = "username"
        
        view.addSubview(profileImageView)
        view.addSubview(usernameLabel)
        view.addSubview(lblFollowing)
        view.addSubview(lblFollowingCount)
        view.addSubview(lblFollower)
        view.addSubview(lblFollowerCount)
        view.addSubview(lblPost)
        view.addSubview(lblPostCount)
        view.addSubview(btnTest)
        
        
        view.backgroundColor = UIColor.cyan
       
        
        profileImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(80)
            make.top.equalTo( self.view.snp.top).offset(8)
            make.left.equalTo( self.view.snp.left).offset(8)
        }
        usernameLabel.snp.makeConstraints{ (make) -> Void in
            make.centerX.equalTo(profileImageView.snp.centerX)
            make.top.equalTo(profileImageView.snp.bottom).offset(14)
        }
        lblFollowing.snp.makeConstraints { (make) in
            make.top.equalTo( self.view.snp.top).offset(40)
            make.right.equalTo( self.view.snp.right).offset(-20)
        }
        lblFollowingCount.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblFollowing.snp.centerY).offset(-18)
            make.centerX.equalTo(lblFollowing.snp.centerX)
        }
        lblFollower.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblFollowing.snp.centerY)
            make.right.equalTo(lblFollowing.snp.left).offset(-20)
        }
        lblFollowerCount.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblFollowing.snp.centerY).offset(-18)
            make.centerX.equalTo(lblFollower.snp.centerX)
        }
        lblPost.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblFollower.snp.centerY)
            make.right.equalTo(lblFollower.snp.left).offset(-20)
        }
        lblPostCount.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblFollowing.snp.centerY).offset(-18)
            make.centerX.equalTo(lblPost.snp.centerX)
        }
        btnTest.snp.makeConstraints { (make) in
            make.centerX.equalTo(lblFollower.snp.centerX)
            make.top.equalTo(lblFollower.snp.bottom).offset(12)
            make.width.equalTo(220)
            make.height.equalTo(20)
        }
        
    }
}
