//
//  MainTabBarController.swift
//  instaclone
//
//  Created by Axl on 20/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase
import OneSignal

class MainTabBarController : UITabBarController , UITabBarControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.delegate = self
        
        if Auth.auth().currentUser == nil {
            
             DispatchQueue.main.async {
                let loginController = LoginController()
                let navController = UINavigationController(rootViewController: loginController)
                
                self.present(navController, animated: true, completion:{
                    
                    UIApplication.shared.statusBarStyle = .lightContent
                })
                
             }
             return
        }
        
        setupViewController()
       // save playerId from oneSignal to -> firebase
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        
        let userID = status.subscriptionStatus.userId
        
        if let playerId = userID {

             
                let userId  = Auth.auth().currentUser?.uid
                
                let db = Database.database().reference().child("users").child(userId!)
                
                let values = ["playerId": playerId ]
                
                db.updateChildValues(values, withCompletionBlock: { (err, _) in
                    if let err = err {
                        print("Error -> update OneSignal playerId -> \(err) ")
                    }
                })
             
           
        }
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let index = viewControllers?.index(of: viewController)
        /*if index == 2 {
            let layout = UICollectionViewFlowLayout()
            let photoSelectorController = PhotoSelectorController(collectionViewLayout:layout)
            let navController = UINavigationController(rootViewController: photoSelectorController)
            
            present(navController, animated: true, completion: nil)
            return false
        }*/
        return true
    }
    public func setupViewController() {
        let layoutHome = UICollectionViewFlowLayout()
        let layoutSearch = UICollectionViewFlowLayout()
        let layoutPhotoSelector = UICollectionViewFlowLayout()
        let userProfileLayout = UICollectionViewFlowLayout()
        let layoutLikeController = UICollectionViewFlowLayout()
        
        
        let homeController = HomeController(collectionViewLayout: layoutHome)
        
        let homeNavController = UINavigationController(rootViewController: homeController)
        
        homeNavController.tabBarItem.image = #imageLiteral(resourceName: "home_unselected")
        homeNavController.tabBarItem.selectedImage = #imageLiteral(resourceName: "home_selected")
        tabBar.tintColor = .black
        
       
        
        let searchNavController = templateNavController(selected: #imageLiteral(resourceName: "search_selected"), unselected: #imageLiteral(resourceName: "search_unselected"), viewController: SearchController(collectionViewLayout: layoutSearch))
        let plusNavController = templateNavController(selected: #imageLiteral(resourceName: "plus_unselected"), unselected: #imageLiteral(resourceName: "plus_unselected") ,viewController: PhotoSelectorController(collectionViewLayout: layoutPhotoSelector))
          let likeNavController = templateNavController(selected: #imageLiteral(resourceName: "like_selected"), unselected:#imageLiteral(resourceName: "like_unselected") ,viewController: LikeController(collectionViewLayout: layoutLikeController))
     
       
        let userProfileController = templateNavController(selected: #imageLiteral(resourceName: "profile_selected"), unselected: #imageLiteral(resourceName: "profile_unselected") ,viewController: UserProfileController(collectionViewLayout: userProfileLayout))
        
        // Chat Controller
      /* let chatController = ChatController()
        chatController.tabBarItem.image = #imageLiteral(resourceName: "comment")
        chatController.tabBarItem.selectedImage = #imageLiteral(resourceName: "comment")
        tabBar.tintColor = .black
        
        let chatNavController = UINavigationController(rootViewController: chatController)
        */
        
        
        
        viewControllers = [homeNavController, searchNavController, plusNavController, likeNavController, userProfileController]
        
        guard let items = tabBar.items else { return }
        
        items.forEach { (item) in
            item.imageInsets = UIEdgeInsetsMake(4, 0, -4, 0)
        }
        
    }
    fileprivate func templateNavController(selected: UIImage, unselected: UIImage, viewController : UIViewController = UIViewController()) -> UINavigationController{
        
       let navController =  UINavigationController(rootViewController: viewController)
        
        navController.tabBarItem.image = unselected
        navController.tabBarItem.selectedImage = selected
        
        return navController
    }
    
}
