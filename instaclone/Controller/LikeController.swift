//
//  UpdateController.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class LikeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var posts = [String:String]()
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = .white
        collectionView?.register(UserProfilePhotoCell.self, forCellWithReuseIdentifier: cellId)
        
          navigationItem.title = "My likes"
        fetchPosts()
        
    }
    fileprivate func fetchPosts(){
        
        guard let userId = Auth.auth().currentUser?.uid else {return}
        
        let dbRef = Database.database().reference().child("likes").child(userId)
        
        dbRef.observe(.value) { (snap) in
            let dic = snap.value as! [String:Any]
            dic.forEach({ (key,value) in
                
                let postId = key as String
                let imageUrl = value as! String
                
                self.posts[postId] = imageUrl
            })
            self.collectionView?.reloadData()
        }
        
        
    }
    
}
extension LikeController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! UserProfilePhotoCell
        
        let imageUrl = Array(posts.values)[indexPath.item] 

        cell.imageView.loadImage(url:imageUrl )
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width - 2) / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
