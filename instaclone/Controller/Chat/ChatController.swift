//
//  ChatController.swift
//  instaclone
//
//  Created by Axl on 23/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import OneSignal
import Firebase
import MessageKit
import Photos

class ChatController: MessagesViewController, MessagesDisplayDelegate, MessagesLayoutDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var user:InstaUser?
    
    var messageToUserId:String?
    
    var messageList: [ChatMessage] = [] {
        didSet {
          DispatchQueue.main.async {
            
                  self.messagesCollectionView.reloadData()
                  self.messagesCollectionView.scrollToBottom()
               }
        }
    }

    let messages: [MessageType] = []
    
    var selectedImage : UIImage?
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        messageInputBar.delegate = self
        
        messageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        
        self.slack()
        
        /// add observer
        guard let userId = Auth.auth().currentUser?.uid else {return}
        let dbRef = Database.database().reference().child("chats").child(userId).child(messageToUserId!).queryOrdered(byChild: "createdTime")
        
        dbRef.observe(.childAdded, with: { (snap) in
            
        

            let messageData = snap.value as! Dictionary<String, Any>

            let messageId = snap.key

            let text = messageData["text"] as! String!
            let createdTime = messageData["createdTime"]
            let senderName = messageData["senderName"]
            let senderId = messageData["senderId"]
            let messageType = messageData["messageType"] as! String!
            let secondsFrom1970 = createdTime as? Double ?? 0
            
            let cretaionDate = Date(timeIntervalSince1970: secondsFrom1970)
            
              let currentSender = Sender(id:senderId as! String  , displayName: senderName as! String)
            if messageType == "text" {
                let attributedText = NSAttributedString(string: text!, attributes: [.font: UIFont.systemFont(ofSize: 15), .foregroundColor: UIColor.blue])
                
              
                
                let message = ChatMessage(attributedText: attributedText, sender: currentSender, messageId:messageId, date: cretaionDate)
                self.messageList.append(message)
         
                
            }else if messageType == "image" {
                let imageUrl = messageData["imageUrl"] as! String
                let message = ChatMessage(image: imageUrl, sender: currentSender, messageId: messageId, date: cretaionDate)
                self.messageList.append(message)
                
 
 
            }
        }) { (err) in
            
            print("error messageInputBar \(err)")
            
        }
        
    }
    
    func slack() {
        messageInputBar.backgroundView.backgroundColor = .white
        messageInputBar.isTranslucent = false
        messageInputBar.inputTextView.backgroundColor = .clear
        messageInputBar.inputTextView.layer.borderWidth = 0
        let items = [
           
            makeButton(named: "capture_photo").onTouchUpInside({ (item) in
                
                item.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
               // self.checkPermission()
                self.selectPicture()
            
                
            }),
            
            .flexibleSpace,
            makeButton(named: "plus_unselected").onTextViewDidChange { button, textView in
                button.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
                button.isEnabled = textView.text.isEmpty
                
            },
            messageInputBar.sendButton
                .configure {
                    $0.layer.cornerRadius = 8
                    $0.layer.borderWidth = 1.5
                    $0.layer.borderColor = $0.titleColor(for: .disabled)?.cgColor
                    $0.setTitleColor(.white, for: .normal)
                    $0.setTitleColor(.white, for: .highlighted)
                    $0.setSize(CGSize(width: 52, height: 30), animated: true)
                }.onDisabled {
                    $0.layer.borderColor = $0.titleColor(for: .disabled)?.cgColor
                    $0.backgroundColor = .white
                }.onEnabled {
                    $0.backgroundColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
                    $0.layer.borderColor = UIColor.clear.cgColor
                }.onSelected {
                    // We use a transform becuase changing the size would cause the other views to relayout
                    $0.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                }.onDeselected {
                    $0.transform = CGAffineTransform.identity
            }
        ]
        items.forEach { $0.tintColor = .lightGray }
        
        // We can change the container insets if we want
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
        
        // Since we moved the send button to the bottom stack lets set the right stack width to 0
        messageInputBar.setRightStackViewWidthConstant(to: 0, animated: true)
        
        // Finally set the items
        messageInputBar.setStackViewItems(items, forStack: .bottom, animated: true)
    }
    // MARK: - Helpers
    
    func makeButton(named: String) -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: named)?.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 30, height: 30), animated: true)
            }.onSelected {
                $0.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
            }.onDeselected {
                $0.tintColor = UIColor.lightGray
            }.onTouchUpInside { _ in
                print("Item Tapped")
              
        }
    }
    
    func selectPicture() {
      
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
        
            self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
        if let chosenImage = info[UIImagePickerControllerEditedImage]{
            sendImage(image: chosenImage as! UIImage)
        }else if let chosenImage = info[UIImagePickerControllerOriginalImage] {
            sendImage(image: chosenImage as! UIImage)
        }

        self.dismiss(animated: true, completion: nil)
        
        
    }
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            //print("Access is granted by user")
            self.selectPicture()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
              //  print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    
                     print("success")
                }
            })
           // print("It is not determined until now")
        case .restricted:
            // same same
             print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }
    fileprivate func sendImage(image: UIImage){
    
    //guard let image = self.selectedImage else {return}
    guard let uploadData = UIImageJPEGRepresentation(image, 0.5) else {return}
  
    let filename = UUID().uuidString
    Storage.storage().reference().child("post_images").child(filename).putData(uploadData, metadata: nil) { (metadata, err) in
    
    if let err = err {
     
        print("Error -> ChatController -> handleShare ",err)
        return
    }
    guard let imageUrl = metadata?.downloadURL()?.absoluteString else {return}
    
    print("imageUrl-> \(imageUrl)")
    // save image to db
    guard let userId = Auth.auth().currentUser?.uid else {return}
    let dbRef = Database.database().reference().child("chats").child(userId).child(self.messageToUserId!)
    
    let itemRef = dbRef.childByAutoId()
    let date = Date()
    let double = date.timeIntervalSince1970
    let messageUID = ("\(double)" + userId).replacingOccurrences(of: ".", with: "")
    
    let values: [String:Any] = ["senderId": userId,"messageId":messageUID,"senderName":self.currentSender().displayName ,"text":"","createdTime":date.timeIntervalSince1970,"messageType":"image","imageUrl":imageUrl]
    
        itemRef.setValue(values)
    
        let dbTo = Database.database().reference().child("chats").child(self.messageToUserId!).child(userId).child(messageUID)
        
        dbTo.setValue(values)
        }
    }

}


extension ChatController: MessagesDataSource {
    
    func currentSender() -> Sender {
       return Sender(id: (self.user?.uid!)!, displayName: (self.user?.username!)!)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? UIColor.orange : UIColor.cyan
    }
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .black : .darkText
    }
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        let avatar = Avatar(initials: (self.user?.username)!)
        avatarView.set(avatar: avatar)
    }
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
        //        let configurationClosure = { (view: MessageContainerView) in}
        //        return .custom(configurationClosure)
    }
}
extension ChatController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        // save message to db
        guard let userId = Auth.auth().currentUser?.uid else {return}
        let date = Date()
        let double = date.timeIntervalSince1970
        let messageUID = ("\(double)" + userId).replacingOccurrences(of: ".", with: "")
        
        let dbRef = Database.database().reference().child("chats").child(userId).child(messageToUserId!).child(messageUID)
        
        let values: [String:Any] = ["senderId": userId,"messageId":messageUID,"senderName":self.currentSender().displayName ,"text":text,"createdTime":date.timeIntervalSince1970,"messageType":"text"]
        
        dbRef.setValue(values)
        
        let dbTo = Database.database().reference().child("chats").child(messageToUserId!).child(userId).child(messageUID)
      
        dbTo.setValue(values)
        inputBar.inputTextView.text = String()

       
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 200
    }
}


