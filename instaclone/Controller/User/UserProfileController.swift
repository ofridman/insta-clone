//
//  UserProfileController.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase

class UserProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    let headerId = "headerId"
    var posts = [Post]()
    var userId : String?
    
    var user: InstaUser? {
        
        didSet{
            self.navigationItem.title =  user?.username
       
            self.fetchOrderedPost()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = UIColor.black
        
        setupLogoutButtom()
        setupCollectionView()
        
        fechtUser()
        
    }
    override func viewDidAppear(_ animated: Bool) {
 
        fechtUser()
        
        
    }
    fileprivate func fechtUser(){

        guard let userId = self.userId ?? Auth.auth().currentUser?.uid else {return}
        
        let userRef = Database.database().reference().child("users").child(userId)
        
        userRef.observeSingleEvent(of: .value, with: { (data) in
            
            guard let dic = data.value as? [String:Any] else { return }
            let instaUser = InstaUser(uid: userId, values: dic)
            self.user = instaUser
            self.collectionView?.reloadData()
          
        }) { (err) in
            print("fechtUser -> ",err)
        }
    }
    fileprivate func fetchOrderedPost(){
        guard let userId = self.user?.uid else { return }
        guard let user = self.user else {return}
        
        let postsRef = Database.database().reference().child("posts").child(userId)
        
        postsRef.queryOrdered(byChild: "createDate").observe(.childAdded, with: { (snap) in
            guard let dic = snap.value as? [String:Any] else {return}
            
            let post = Post(user: user, dictionary: dic, postId:snap.key)
            self.posts.insert(post, at: 0)
            self.collectionView?.reloadData()
        }) { (err) in
            print("Error -> UserProfileController -> fetchOrderedPost -> ",err)
        }
        
    }
    fileprivate func fetchPosts(){
        guard let userId = self.user?.uid else { return }
        guard let user = self.user else {return}
        
        let postsRef = Database.database().reference().child("posts").child(userId)
        
        postsRef.queryOrdered(byChild: "createDate").observeSingleEvent(of: .value, with: { (snap) in
            
            guard let dics = snap.value as? [String:Any] else {return}
            dics.forEach({ (key,value) in
                guard let dic = value as? [String:Any] else {return}
                
                let post = Post(user: user, dictionary: dic,postId:snap.key)
                self.posts.append(post)
                self.collectionView?.reloadData()
            })
            
        }) { (err) in
            print("Error -> UserProfileController -> fetchPosts ->",err )
        }
    }
    fileprivate func setupCollectionView(){
        collectionView?.backgroundColor = .white
        collectionView?.register(UserProfilePhotoCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView?.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader , withReuseIdentifier: headerId)
        
        collectionView?.alwaysBounceVertical = true
        
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! UserProfileHeader
    
        header.user = self.user
        
        
     return header
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width - 2) / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! UserProfilePhotoCell

        cell.post = self.posts[indexPath.item]
        
         
        return cell
    }
    func setupLogoutButtom(){
       // let logoutButtom = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        
        let settingButton = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showAlert))
        
        
        navigationItem.rightBarButtonItem = settingButton
        
    }
    @objc func showAlert(){
        let alertController = UIAlertController(title: nil, message: nil
            , preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (_) in
            self.handleLogout()
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alertController, animated: true, completion: nil)
        
    }
    @objc func handleLogout(){
        do {
            try Auth.auth().signOut()
            let loginController = LoginController()
            let navController = UINavigationController(rootViewController: loginController)
            self.present(navController, animated: true, completion: nil)
        }
        catch let err{
            print("Error to logout " , err)
        }
        
    }
}
