//
//  EditProfileVC.swift
//  instaclone
//
//  Created by Axl on 25/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import SnapKit
import Firebase
import FirebaseAuth
import FirebaseStorage

class EditProfileController : UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    lazy var profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.image = #imageLiteral(resourceName: "plus_photo")
        iv.contentMode = .scaleToFill
        iv.clipsToBounds = true
        iv.isUserInteractionEnabled = true
        iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleProfilePhoto)))
        return iv
    }()
    let emailTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Email"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        //tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)
        return tf
    }()
    
    let userTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Username"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
       // tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)
        return tf
    }()
    let signupButton : UIButton = {
        let bt = UIButton(type: UIButtonType.system)
        bt.setTitle("Save", for: .normal)
        bt.setTitleColor(UIColor.white, for: .normal)
        bt.backgroundColor = UIColor.lightGray
       // bt.addTarget(self, action: #selector(handleSignup), for: .touchUpInside)
        bt.layer.cornerRadius = 5
        bt.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        bt.isEnabled = false
        return bt
    }()
    override func viewDidLoad() {
          self.view.autoresizingMaskIntoSubviews()
          setupView()
        
          guard let userId = Auth.auth().currentUser?.uid else {return}
        
            let dbRef = Database.database().reference().child("users").child(userId)
        
        dbRef.observe(.value) { (snap) in
            
                let dic = snap.value as! [String:Any]
            
                if let imageUrl  = dic["profileImageUrl"] as? String {
                
                self.profileImageView.loadImage(url: imageUrl)
                self.userTextField.text = dic["username"] as? String
                   
                
                }
            
        }
        
    }
    
    func setupView() {
        view.backgroundColor = .white
        let btnLeft = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        
        navigationItem.title = "Edit Profile"
        navigationItem.leftBarButtonItem = btnLeft;
        
        let btnSave = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
        
        navigationItem.rightBarButtonItem = btnSave
        
        view.addSubview(profileImageView)
        
        profileImageView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top).offset(88)
            make.centerX.equalTo(view.snp.centerX)
            make.height.width.equalTo(150)
            
        }
        
        profileImageView.layer.cornerRadius = 150/2
        
        view.addSubview(userTextField)
        
        userTextField.snp.makeConstraints { (make) in
            make.top.equalTo(profileImageView.snp.bottom).offset(40)
            
            make.left.equalTo(view.snp.left).offset(20)
            make.right.equalTo(view.snp.right).offset(-20)
            
        }
        
    }
    @objc func handleCancel(){
        
        self.dismiss(animated: true, completion: nil)
    }
    @objc func handleSave(){
        
        guard  let image = self.profileImageView.image else { return }
        guard let uploadData = UIImageJPEGRepresentation(image, 0.3) else { return }
        let filename = Auth.auth().currentUser?.uid
        
        let storageRef = Storage.storage().reference().child("profile_images").child(filename!)
        
        Helper.shared.showLoading(view: self.view)
        storageRef.putData(uploadData, metadata: nil) { (data, err) in
            
            if let err = err {
                print("Error upload profile image ", err)
                
            }else {
                guard let userId = Auth.auth().currentUser?.uid  else { return }
                guard let imageUrl = data?.downloadURL()?.absoluteString else { return }
                
                let values: [String: Any] = ["username":self.userTextField.text!,"profileImageUrl": imageUrl]
                let userRef = Database.database().reference().child("users").child(userId)
               
            
                
                userRef.updateChildValues(values, withCompletionBlock: { (err, _) in
                    if let err = err {
                        print("error update user " , err)
                        
                    }else {
                        Helper.shared.myView.removeFromSuperview()
                       self.dismiss(animated: true, completion: nil)
                        
                    }
                })
            } // end else
        } // end putdata
    }
    @objc func handleProfilePhoto(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate =  self
        imagePickerController.allowsEditing = true
        
        present(imagePickerController, animated: true, completion: nil)
         
    }
    
}
extension EditProfileController {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            self.profileImageView.image = editedImage
        } else  if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            self.profileImageView.image = originalImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
