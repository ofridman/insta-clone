//
//  SearchController.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SearchController: UICollectionViewController , UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    let cellid = "cellId"
    var isForChat = false
    var currentUser: InstaUser?
    var users = [InstaUser]()
    //var filteredUser = [InstaUser]()
   
    lazy var searchBar : UISearchBar = {
       let sb = UISearchBar()
        sb.placeholder = "Enter username..."
        sb.delegate = self
        return sb
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        searchBar.sizeToFit()
        navigationItem.titleView = searchBar
        navigationController?.navigationBar.tintColor = UIColor.black
        
        collectionView?.backgroundColor = .white
        collectionView?.register(UserSearchCell.self, forCellWithReuseIdentifier: cellid)
        collectionView?.alwaysBounceVertical = true
        collectionView?.keyboardDismissMode = .onDrag
        
        if isForChat == false {
            fetchUser()
        }else {
            fetchUser()
        }
    
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
             fetchUser()
            
        }else {
            self.users = self.users.filter({ (user) -> Bool in
                
                return  (user.username?.lowercased().contains(searchText.lowercased()))!
            })
            self.collectionView?.reloadData()
        }
    }
    fileprivate func fetchFriends(){
        guard let currentUserId = Auth.auth().currentUser?.uid else {return}
        let userRef = Database.database().reference().child("followers").child(currentUserId)
        
        userRef.observe(.value) { (_) in
             //guard let dic = snap.value  as? [String:Any] else {return}
        }
        
        
        
    }
    fileprivate func fetchUser(){

            guard let currentUserId = Auth.auth().currentUser?.uid else {return}
            
            let userRef = Database.database().reference().child("users")
        
        
            userRef.observeSingleEvent(of: .value, with: { (data) in
                self.users.removeAll()
                guard let dics = data.value  as? [String:Any] else {return}
       
                dics.forEach({ (key,value) in
                    if key != currentUserId {
                        guard let dic = value as? [String:Any] else {return}
                        let user = InstaUser(uid: key, values: dic)
                        self.users.append(user)
                    }
                })
                self.collectionView?.reloadData()
            }) { (err) in
                print("Error -> SearchController -> fetchUser-> ",err)
            }
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isForChat == false {
            
            let layout = UICollectionViewFlowLayout()
            let userProfileController = UserProfileController(collectionViewLayout: layout)
            userProfileController.userId = self.users[indexPath.item].uid
            
            navigationController?.pushViewController(userProfileController, animated: true)
        }else {
        
                let chatViewController = ChatController()
                chatViewController.user = self.currentUser
                chatViewController.messageToUserId = self.users[indexPath.item].uid
               
               
                self.collectionView?.deselectItem(at: indexPath, animated: true)
                self.collectionView?.isUserInteractionEnabled = true
                self.navigationController?.pushViewController(chatViewController, animated: true)
                /*
                if  let toChatUserId = self.users[indexPath.item].uid {
                    let allUpdates =  ["/users/\(Me.uid)/contacts/\(toChatUserId)": (["username": self.users[indexPath.item].username]),
                                       "/users/\(toChatUserId)/contacts/\(Me.uid)": (["username": currentUser?.username])]
                    
                    Database.database().reference().updateChildValues(allUpdates)
                }
                   */
           
        }
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.users.count

    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! UserSearchCell
   
            cell.user = self.users[indexPath.item]
    
             return cell
    }  
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width
        let height: CGFloat = 66
        
        return CGSize(width: width, height: height)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
  
  
}
 

