//
//  HomeController.swift
//  instaclone
//
//  Created by Axl on 20/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase
import OneSignal

class HomeController: UICollectionViewController,UICollectionViewDelegateFlowLayout {
    
    var posts = [Post]()
    let cellId = "cellId"
    var currentUser : InstaUser? {
        didSet{
            guard let currentUser = currentUser else {return}
            self.fetchPosts(user: currentUser)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // Helper.shared.showLoading(view: self.view)
        
        /* //Timer for test purpose
        var timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { timer in
            
            Helper.shared.myView.removeFromSuperview()
        }
         */

        
        // save PlayerId from Onesginal
        savePlayerIdOneSignal()
        
        
        
       // self.collectionView?.collectionViewLayout.invalidateLayout()
        collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = .white
        
        self.view.autoresizingMaskIntoSubviews()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        setupNavBar()
        fetchAllPost()
    }
    fileprivate func savePlayerIdOneSignal(){
        // save the playerId from onesignal
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        
        let playerId =  status.subscriptionStatus.userId
        
        
        if let playerId = playerId {
            // save the  playerId on Db
            guard let userId = Auth.auth().currentUser?.uid else {return}
            let dbRef = Database.database().reference().child("users").child(userId).child("playerId")
            dbRef.observeSingleEvent(of: .value, with: { (snap) in
                if snap.exists() == false {
                    
                    let db = Database.database().reference().child("users").child(userId)
                    let values: [String:Any] = ["playerId":playerId]
                    db.updateChildValues(values)
                }
            })
            
        }
    }
   
    func setupNavBar(){
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logo2"))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "camera3").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCapturePhoto))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "send2").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleChat))
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        if currentUser == nil {
            InstaUser.fecthUser(uid: (Auth.auth().currentUser?.uid)!, completion: { (user) in
                self.currentUser = user
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            })
        }
    }
    @objc func handleRefresh(){
        
        //self.collectionView?.collectionViewLayout.invalidateLayout()
        //collectionView?.reloadSections(1)
        self.posts.removeAll()
        self.fetchAllPost()
   
    }
    @objc func handleCapturePhoto(){
        
    }
    @objc func handleChat(){
        
        let layout = UICollectionViewFlowLayout()
        
        let searchViewController = SearchController(collectionViewLayout: layout)
        
        searchViewController.currentUser = self.currentUser
        searchViewController.isForChat = true
        navigationController?.pushViewController(searchViewController, animated: true)
        
    }
    fileprivate func fetchAllPost(){

        guard let userId = Auth.auth().currentUser?.uid  else { return }
        
        InstaUser.fecthUser(uid: userId) { (user) in
            self.currentUser = user
        }
        fetchFollowingUserId()
       
    }
    fileprivate func fetchFollowingUserId(){
        guard let currentUserId = Auth.auth().currentUser?.uid else {return}
        let db = Database.database().reference().child("following").child(currentUserId)
        
        db.observeSingleEvent(of: .value, with: { (data) in
            guard let userIdsDic = data.value as? [String:Any] else {return}
            
            userIdsDic.forEach({ (key,value) in
                InstaUser.fecthUser(uid: key, completion: { (user) in
                    self.fetchPosts(user:user)
                })
            })
            
        }) { (err) in
            print("Error ->homeVC->fetchFollowingUserId->",err)
        }
    }
    fileprivate func fetchPosts(user:InstaUser){
        
        
        let postsRef = Database.database().reference().child("posts").child(user.uid!)
        
        postsRef.queryOrdered(byChild: "createDate").observeSingleEvent(of: .value, with: { (snap) in
            self.collectionView?.refreshControl?.endRefreshing()
            guard let dics = snap.value as? [String:Any] else {return}
            
           
            dics.forEach({ (key,value) in
                guard let dic = value as? [String:Any] else {return}
  
                let post = Post(user: user, dictionary: dic, postId: key)
                self.posts.append(post)
                
                self.posts.sort(by: { (p1, p2) -> Bool in
                    return p1.cretaionDate.compare(p2.cretaionDate) == .orderedDescending
                })
                Helper.shared.myView.removeFromSuperview()
                self.collectionView?.reloadData()
      
            })
            
        }) { (err) in
            print("Error -> homeVC -> fetchPosts ->",err )
        }
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomePostCell
        if self.posts.count > 0 {
            cell.post = self.posts[indexPath.item]
            cell.setupCell()
        }
       
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width =  view.frame.width
        let height = width + 166
        return CGSize(width: width, height: height)
    }
 
}
