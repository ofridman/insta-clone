//
//  LoginController.swift
//  instaclone
//
//  Created by Axl on 20/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController {
    
    let topBackgroundView : UIView = {
        let view = UIView()
        view.backgroundColor = ColorCodes.instagramBlue
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo_white"))
        logoImageView.contentMode = .scaleAspectFit
        
        view.addSubview(logoImageView)
        logoImageView.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 200, height: 50)
        
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
        
        return view
    }()
    
    let emailTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Email"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.keyboardType = .emailAddress
        tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)
        return tf
    }()
    
    let passTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Password"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)

        return tf
    }()
    let loginButton : UIButton = {
        let bt = UIButton(type: UIButtonType.system)
        bt.setTitle("Login", for: .normal)
        bt.setTitleColor(UIColor.white, for: .normal)
        bt.backgroundColor = UIColor.lightGray
        bt.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        bt.layer.cornerRadius = 5
        bt.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        bt.isEnabled = false
        return bt
    }()
    let noAccountButton: UIButton = {
        let bt = UIButton(type: UIButtonType.system)
        
        let attributedString = NSMutableAttributedString(string: "Don't have an account? ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.lightGray ])
        attributedString.append(NSMutableAttributedString(string: " Sign up", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: ColorCodes.instagramBlue ]) )
       
        bt.setAttributedTitle(attributedString, for: .normal)
        bt.addTarget(self, action: #selector(handleShowSignup), for: .touchUpInside)
        return bt
    }()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
         navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .white
        view.addSubview(topBackgroundView)
        view.addSubview(emailTextField)
        view.addSubview(passTextField)
        view.addSubview(loginButton)
        view.addSubview(noAccountButton)
        
      
        topBackgroundView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 150)
        
        emailTextField.anchor(top: topBackgroundView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 25, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 40)

        passTextField.anchor(top: emailTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 40)
        
        
        loginButton.anchor(top: passTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 25, paddingBottom: 8, paddingRight: 25, width: 0, height: 50)
        
        if #available(iOS 11.0, *) {
            noAccountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingTop:0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 50)
        } else {
            // Fallback on earlier versions
            noAccountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop:0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 50)
        }
        
    }
    @objc func handleLogin(){
        
        
        guard let email = emailTextField.text, let pass = passTextField.text else { return }
        
        loginButton.isEnabled = false
        loginButton.setTitle("Loading...", for: .normal)
        loginButton.backgroundColor = UIColor.lightGray
        
         
        Auth.auth().signIn(withEmail: email, password: pass) { (user, err) in
            if let err = err {
                print("Login error ", err)
                self.loginButton.isEnabled = true
                self.loginButton.setTitle("Login", for: .normal)
                self.loginButton.backgroundColor = ColorCodes.instagramBlue
            }else {
                
                guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else {return}
                
                mainTabBarController.setupViewController()
                
                
                self.dismiss(animated: true, completion: {
                    
                    UIApplication.shared.statusBarStyle = .default
                })
            }
        }
    }
    @objc func handleShowSignup(){
        let signupController = SignupController()
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.pushViewController(signupController, animated: true)
        
    }
    @objc func handleInputChange (){
        if emailTextField.text?.count ?? 0 > 0 && passTextField.text?.count ?? 0 > 0 {
            self.loginButton.backgroundColor = ColorCodes.instagramBlue
            self.loginButton.isEnabled = true
        }else {
            self.loginButton.backgroundColor = UIColor.lightGray
            self.loginButton.isEnabled = false
        }
    }
}

