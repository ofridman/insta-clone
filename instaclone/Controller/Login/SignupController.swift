//
//  SignupController.swift
//  instaclone
//
//  Created by Axl on 20/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase

class SignupController: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    lazy var plusImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "plus_photo")
        iv.contentMode = .scaleToFill
        iv.clipsToBounds = true
        iv.isUserInteractionEnabled = true
        iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePlusPhoto)))
        return iv
    }()

    let emailTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Email"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)
        return tf
    }()
    
    let passTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Password"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)
        return tf
    }()
    let userTextField : UITextField = {
        let tf = UITextField ()
        tf.placeholder = "Username"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleInputChange), for: .editingChanged)
        return tf
    }()
    let signupButton : UIButton = {
        let bt = UIButton(type: UIButtonType.system)
        bt.setTitle("Sign up", for: .normal)
        bt.setTitleColor(UIColor.white, for: .normal)
        bt.backgroundColor = UIColor.lightGray
        bt.addTarget(self, action: #selector(handleSignup), for: .touchUpInside)
        bt.layer.cornerRadius = 5
        bt.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        bt.isEnabled = false
        return bt
    }()
    let accountButton: UIButton = {
        let bt = UIButton(type: UIButtonType.system)
        
        let attributedString = NSMutableAttributedString(string: "Already have an account? ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.lightGray ])
        attributedString.append(NSMutableAttributedString(string: " Login", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: ColorCodes.instagramBlue ]) )
        
        bt.setAttributedTitle(attributedString, for: .normal)
        bt.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return bt
    }()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .white
        view.addSubview(plusImageView)
        view.addSubview(emailTextField)
        view.addSubview(userTextField)
        view.addSubview(passTextField)
        view.addSubview(signupButton)
        view.addSubview(accountButton)
        
      
        plusImageView.anchor(top: view.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 50, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 150, height: 150)
        
        plusImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        plusImageView.layer.cornerRadius = 150/2
        
        
        emailTextField.anchor(top: plusImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 25, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 40)
        
        userTextField.anchor(top: emailTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 40)
        
        passTextField.anchor(top: userTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 40)
        
        
        signupButton.anchor(top: passTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 25, paddingBottom: 8, paddingRight: 25, width: 0, height: 50)
        
        if #available(iOS 11.0, *) {
            accountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingTop:0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 50)
        } else {
            // Fallback on earlier versions
            accountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop:0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 50)
        }
        
    }
    @objc func handleLogin(){
        UIApplication.shared.statusBarStyle = .lightContent
       self.navigationController?.popViewController(animated: true)
    }
    @objc func handleSignup(){
        guard let email = emailTextField.text , let username = userTextField.text, let pass = passTextField.text else { return }
      
        signupButton.isEnabled = false
        signupButton.setTitle("Loading...", for: .normal)
        signupButton.backgroundColor = UIColor.lightGray
        
        Auth.auth().createUser(withEmail: email, password: pass) { (user, error) in
            if let error = error {
                print("Failed to create user ",error)
                self.signupButton.isEnabled = true
                self.signupButton.setTitle("Sign up", for: .normal)
                self.signupButton.backgroundColor = ColorCodes.instagramBlue
            }else{
                
                guard  let image = self.plusImageView.image else { return }
                guard let uploadData = UIImageJPEGRepresentation(image, 0.3) else { return }
                let filename = user?.uid
                
                let storageRef = Storage.storage().reference().child("profile_images").child(filename!)
                
                storageRef.putData(uploadData, metadata: nil) { (data, err) in
                    
                    if let err = err {
                        print("Error upload profile image ", err)
                        
                    }else {
                        guard let userId = user?.uid  else { return }
                        guard let imageUrl = data?.downloadURL()?.absoluteString else { return }
                        
                        let values: [String: Any] = ["username":username,"email": email,"profileImageUrl": imageUrl]
                        let userRef = Database.database().reference().child("users").child(userId)
                        let changeRequest = user!.createProfileChangeRequest()
                        changeRequest.displayName = username
                        changeRequest.photoURL = data?.downloadURL()
                        changeRequest.commitChanges(completion: nil)
                        
                        userRef.updateChildValues(values, withCompletionBlock: { (err, _) in
                            if let err = err {
                                print("error update user " , err)
                                
                            }else {
                                guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else {return}
                                
                                   mainTabBarController.setupViewController()
                                  self.dismiss(animated: true, completion: {
                                     UIApplication.shared.statusBarStyle = .default
                                  })
                            }
                        })
                    }
                        
                    
                   
                }
                
              
            } // end else
        }
        
    }
    @objc func handlePlusPhoto(){
       let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        present(imagePickerController, animated: true, completion: nil)
        
        
    }
    @objc func handleInputChange (){
        if emailTextField.text?.count ?? 0 > 0 && passTextField.text?.count ?? 0 > 0 && userTextField.text?.count ?? 0 > 0 {
            self.signupButton.backgroundColor = ColorCodes.instagramBlue
            self.signupButton.isEnabled = true
        }else {
            self.signupButton.backgroundColor = UIColor.lightGray
            self.signupButton.isEnabled = false
        }
    }
}
extension SignupController {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
             self.plusImageView.image = editedImage
        } else  if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            self.plusImageView.image = originalImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

