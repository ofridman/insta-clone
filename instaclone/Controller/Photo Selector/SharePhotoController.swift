//
//  SharePhotoController.swift
//  instaclone
//
//  Created by Axl on 22/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase

class SharePhotoController: UIViewController {
    
    var selectedImage: UIImage?{
        didSet{
            self.imageView.image = selectedImage
        }
    }
    
    let imageView : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .red
        
        return iv
    }()
    
    let textView : UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.layer.borderColor = UIColor.lightGray.cgColor
        tv.layer.borderWidth = 1
        tv.layer.cornerRadius = 3
        return tv
    }()
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor =  UIColor(white: 0.95, alpha: 1)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(handleShare))
        
        setupView()
    }
    @objc func handleShare(){
        
        Helper.shared.showLoading(view: self.view)
        
        guard let caption = textView.text, caption.count > 0 else { return }
        
        guard let image = selectedImage else {return}
        guard let uploadData = UIImageJPEGRepresentation(image, 0.5) else {return}
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let filename = UUID().uuidString
        
        
        Storage.storage().reference().child("post_images").child(filename).putData(uploadData, metadata: nil) { (metadata, err) in
            
            if let err = err {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                
                print("Error -> SharePhotoController -> handleShare ",err)
                return
            }
            guard let imageUrl = metadata?.downloadURL()?.absoluteString else {return}
            self.savePostToDatabase(withUrl: imageUrl)
        }
        
        
    }
    fileprivate func savePostToDatabase(withUrl:String){
        guard let currentUserId = Auth.auth().currentUser?.uid else {return}
        guard let caption = textView.text else { return }
        guard let image = selectedImage else { return }
        let postRef = Database.database().reference().child("posts").child(currentUserId)
        let ref = postRef.childByAutoId()
        
        let values: [String:Any] = ["imageUrl":withUrl,"caption":caption,"creationDate":Date().timeIntervalSince1970 ,"imageWidth":image.size.width, "imageHeight":image.size.height]
        
        ref.updateChildValues(values) { (err, _) in
            if let err = err {
                print("Error -> SharePhotoController -> savePostToDatabase -> ",err )
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                Helper.shared.myView.removeFromSuperview()
                return
            }
            Helper.shared.myView.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        }

        
        
        
        
    }
    func setupView(){
        
        let containerView = UIView()
        
        containerView.backgroundColor = .white
        
        view.addSubview(containerView)
        
        if #available(iOS 11.0, *) {
            containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 120)
        } else {
            // Fallback on earlier versions
             containerView.anchor(top: topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 120)
        }
        containerView.addSubview(imageView)
        containerView.addSubview(textView)
        
        imageView.anchor(top: nil, left: containerView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 80, height: 80)
        imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
       textView.anchor(top: imageView.topAnchor, left: imageView.rightAnchor, bottom: imageView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 8, width: 0, height: 0)
        
        
      
        
    }
}
