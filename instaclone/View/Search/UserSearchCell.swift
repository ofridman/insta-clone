//
//  UserSearchCell.swift
//  instaclone
//
//  Created by Axl on 22/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import SnapKit

class UserSearchCell: BaseCell {
    
    var user: InstaUser? {
        didSet{
            
            guard let user = user else {return}
            profileImageView.loadImage(url: user.profileImageUrl!)
            usernameLabel.text = user.username
        }
    }
    let profileImageView: CustomImageView = {
       let iv = CustomImageView()
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 50/2
       return iv
    }()
    let usernameLabel: UILabel = {
       let lbl = UILabel()
        lbl.text = "username"
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        return lbl
    }()
    let lblDate: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor(red:0.5, green:0.5, blue:0.5, alpha:1.0)
        return lbl
    }()
    let lblChat: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont.systemFont(ofSize: 11)
        lbl.textColor = UIColor(red:0.5, green:0.5, blue:0.5, alpha:1.0)
        return lbl
    }()
    override func setupCell() {
        super.setupCell()
        
        backgroundColor = .white
        addSubview(profileImageView)
        addSubview(usernameLabel)
        addSubview(lblChat)
        addSubview(lblDate)
        
        profileImageView.snp.makeConstraints { (make) in
            make.left.equalTo(snp.left).offset(10)
            make.height.width.equalTo(50)
            make.centerY.equalTo(snp.centerY)
            
        }
        
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(snp.top)
            make.left.equalTo(profileImageView.snp.right).offset(10)
            make.bottom.equalTo(snp.bottom)
            make.right.equalTo(snp.right)
            
        }
        lblChat.snp.makeConstraints { (make) in
            make.top.equalTo(usernameLabel.snp.top).offset(30)
            make.left.equalTo(usernameLabel.snp.left)
            make.bottom.equalTo(snp.bottom)
            make.right.equalTo(snp.right)
        }
        
        lblDate.snp.makeConstraints { (make) in
            make.centerY.equalTo(snp.centerY)
            make.right.equalTo(snp.right).offset(-20)
            
        }
        
    
        let separatorView = UIView()
        separatorView.backgroundColor = .lightGray
        addSubview(separatorView)
       
        
        separatorView.snp.makeConstraints { (make) in
            make.left.equalTo(snp.left)
            make.bottom.equalTo(snp.bottom)
            make.right.equalTo(snp.right)
            make.height.equalTo(0.5)
        }
    }
}
