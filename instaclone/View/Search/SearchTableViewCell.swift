//
//  SearchTableViewCell.swift
//  instaclone
//
//  Created by Axl on 25/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit

class SearchTableViewCell:  UITableViewCell {

    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
    func setupCell(){
        profileImageView.layer.cornerRadius = 50/2
        
    }
    
    
    
}
