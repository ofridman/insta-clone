//
//  UserProfilePhotoCell.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit

class UserProfilePhotoCell: BaseCell {
    
    var post: Post? {
        didSet{
            guard let imageUlr = post?.imageUrl else {return}
            imageView.loadImage(url: imageUlr)
        }
    }
    
    let imageView : CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        //iv.backgroundColor = .orange
        
        return iv
    }()
 
    override func setupCell() {
        
        super.setupCell()
       
        addSubview(imageView)
        
        imageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
 
}
class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCell(){
        
    }
}
