//
//  UserProfileHeader.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase
import SnapKit

class UserProfileHeader: BaseCell {
    var user: InstaUser? {
        didSet{
            self.usernameLabel.text = user?.username
            guard let profileImageUrl = user?.profileImageUrl else {return}
            profileImageView.loadImage(url: profileImageUrl)
            setupEditOrFollowButtom()
            setupFollowersFollowing()
        }
    }
    var following:UInt? {
        
        didSet{
            setupFollowing()
        }
    }
    var follower:UInt? {
        
        didSet{
            setupFollower()
        }
    }
    var posts:UInt? {
        
        didSet{
            setupPosts()
        }
    }
    let profileImageView : CustomImageView = {
        let iv = CustomImageView()
        iv.backgroundColor = UIColor(white: 0.9, alpha: 0.8)
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 80/2
        return iv
    }()
    let postLabel: UILabel = {
        let lbl = UILabel()
        let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])

        attributedText.append(NSMutableAttributedString(string: "posts", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor:UIColor.lightGray]))
     
        lbl.attributedText = attributedText
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        
        return lbl
    }()
    let followerLabel: UILabel = {
        let lbl = UILabel()
        let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14) ])
        
        attributedText.append(NSMutableAttributedString(string: "followers", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor:UIColor.lightGray]))
        
        lbl.attributedText = attributedText
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()
    let followingLabel: UILabel = {
        let lbl = UILabel()
        let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14) ])
        
        attributedText.append(NSMutableAttributedString(string: "following", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor:UIColor.lightGray]))
        
        lbl.attributedText = attributedText
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()
    lazy var editFollowButton: UIButton = {
       
        let btn = UIButton(type:.system)
        btn.setTitle("Edit Profile", for: .normal)
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth  = 1
        btn.layer.cornerRadius = 3
        btn.addTarget(self, action: #selector(handleEditFollowButton), for: .touchUpInside)
        return btn
    }()
    let gridButton: UIButton = {
        
        let btn = UIButton(type:.system)
        btn.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
        btn.tintColor = UIColor(white: 0, alpha: 1)
        return btn
    }()
    let listButton: UIButton = {
        
        let btn = UIButton(type:.system)
        btn.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        btn.tintColor = UIColor(white: 0, alpha: 0.2)
        return btn
    }()
    let bookmarkButton: UIButton = {
        
        let btn = UIButton(type:.system)
        btn.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
        btn.tintColor = UIColor(white: 0, alpha: 0.2)
        return btn
    }()
    let usernameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "username"
        lbl.textColor = UIColor.black
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        return lbl
    }()
    fileprivate func setupFollowing(){
       // guard let post = post else  {return}
        let followingAttributedText = NSMutableAttributedString(string: "\(following ?? 0)\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14) ])
        
        followingAttributedText.append(NSMutableAttributedString(string: "following", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor:UIColor.lightGray]))
       
        followingLabel.attributedText = followingAttributedText
        
      /*  let attributedString = NSMutableAttributedString(string: post.user.username!, attributes: [NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 14) ])
        
        attributedString.append(NSMutableAttributedString(string: " \(post.caption)", attributes: [NSAttributedStringKey.font :UIFont.systemFont(ofSize: 14) ]))
        attributedString.append(NSMutableAttributedString(string: "\n\n", attributes: [NSAttributedStringKey.font :UIFont.systemFont(ofSize: 4) ]))
        
        let creationString = post.cretaionDate.timeAgoDisplay()
        
        attributedString.append(NSMutableAttributedString(string: "\(creationString)", attributes: [NSAttributedStringKey.font :UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor :UIColor.lightGray  ]))
        
        postLabel.attributedText = attributedString */
        
    }
    fileprivate func setupFollower(){
        // guard let post = post else  {return}
        let followerAttributedText = NSMutableAttributedString(string: "\(follower ?? 0)\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14) ])
        
        followerAttributedText.append(NSMutableAttributedString(string: "follower", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor:UIColor.lightGray]))
        
        followerLabel.attributedText = followerAttributedText

    }
    fileprivate func setupPosts(){
        // guard let post = post else  {return}
        let postsAttributedText = NSMutableAttributedString(string: "\(posts ?? 0)\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14) ])
        
        postsAttributedText.append(NSMutableAttributedString(string: "posts", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor:UIColor.lightGray]))
        
        postLabel.attributedText = postsAttributedText
        
    }
    fileprivate func setupEditOrFollowButtom(){
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else {return}
        
        guard let  userId = user?.uid else  {return}
        
        if currentLoggedInUserId == userId {
            
        }else {
            // check is already following
            let followRef = Database.database().reference().child("following").child(currentLoggedInUserId).child(userId)
            
                followRef.observe(.value, with: { (data) in
                
                
                if let isFollowing = data.value as? Int, isFollowing == 1 {
                    // unfollow button
                   self.editFollowButton.setTitle("Unfollow", for: .normal)
                }else {
                   self.setupFollowButton()
                }
                
            }, withCancel: { (err ) in
                print("Error->UserProfileHeader->setupEditOrFollowButtom->",err)
            })
            
            
        }
        
    }
    fileprivate func setupFollowButton(){
         self.editFollowButton.setTitle("Follow", for: .normal)
         self.editFollowButton.backgroundColor = ColorCodes.instagramBlue
         self.editFollowButton.setTitleColor(UIColor.white, for: .normal)
        
    }
    @objc func handleEditFollowButton(){
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else {return}
        
        guard let  userId = user?.uid else  {return}
        
        if currentLoggedInUserId == userId {
            
            let editViewController = EditProfileController()
         
            let editNavController = UINavigationController(rootViewController: editViewController)
            
           editNavController.navigationBar.tintColor = .black

            self.window?.rootViewController?.present(editNavController, animated: true, completion: nil)
            

        }else {
            if self.editFollowButton.titleLabel?.text == "Unfollow" {
                // remove uiser id form database -> unfollow
                 let followingRef = Database.database().reference().child("following").child(currentLoggedInUserId).child(userId)
                     followingRef.removeValue(completionBlock: { (err , _) in
                    
                    if let err = err {
                        print("Error->UserProfileHeader->handleEditFollowButton->removeValue->",err)
                        return
                    }
                    self.setupFollowButton()
                })
                
                // remove from follower
                let followerRef = Database.database().reference().child("follower").child(userId).child(currentLoggedInUserId)
                followerRef.removeValue()
                
            }else {
                // follow userId
                let values: [String:Any] = [userId:1]

                let followingRef = Database.database().reference().child("following").child(currentLoggedInUserId)
                
                   followingRef.updateChildValues(values, withCompletionBlock: { (err, _) in
                    if let err = err {
                        print("Error ->UserProfileHeader->handleEditFollowButton-> ",err)
                    }
                    self.editFollowButton.setTitle("Unfollow", for: .normal)
                    self.editFollowButton.backgroundColor = .white
                    self.editFollowButton.setTitleColor(UIColor.black, for: .normal)
                    
                })
                
                // add follower to db
                let followerRef = Database.database().reference().child("follower").child(userId)
                  let tempValues: [String:Any] = [currentLoggedInUserId:1]
                    followerRef.updateChildValues(tempValues)
                
                
            }
            
            
            
        }
        
    }
    override func setupCell() {
        super.setupCell()
        setupView()
    }
    fileprivate func setupFollowersFollowing(){
    guard let  userId = user?.uid else  {return}
  
        let followingRef = Database.database().reference().child("following").child(userId)
        
        followingRef.observeSingleEvent(of: .value) { (snap) in
        self.following = snap.childrenCount
        }
        
        let followerRef = Database.database().reference().child("follower").child(userId)
        
        followerRef.observeSingleEvent(of: .value) { (snap) in
        self.follower = snap.childrenCount
        }
        let postsRef = Database.database().reference().child("posts").child(userId)
        
        postsRef.observeSingleEvent(of: .value) { (snap) in
            self.posts = snap.childrenCount
        }
    
    }
    fileprivate func setupView(){
        addSubview(profileImageView)
        addSubview(usernameLabel)
       
        
        
        profileImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(80)
            make.top.equalTo(snp.top).offset(8)
            make.left.equalTo(snp.left).offset(8)
        }
        usernameLabel.snp.makeConstraints{ (make) -> Void in
            make.centerX.equalTo(profileImageView.snp.centerX)
            make.top.equalTo(profileImageView.snp.bottom).offset(14)
        }
        setupLabelStackView()
        setupBottomBar()
    
    }
    fileprivate func setupLabelStackView(){
        
        let stackView = UIStackView(arrangedSubviews: [postLabel,followerLabel,followingLabel])
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            
            make.height.equalTo(50)
            make.width.equalTo(250)
            make.top.equalTo(snp.top)
            make.right.equalTo(snp.right).offset(-20)
        }
 
        addSubview(editFollowButton)
        
        editFollowButton.snp.makeConstraints { (make) in
            
            make.top.equalTo(stackView.snp.bottom).offset(4)
            make.left.equalTo(stackView.snp.left).offset(20)
            
            make.width.equalTo(220)
            make.height.equalTo(24)
        }
        
    
        
    }
    fileprivate func setupBottomBar(){
        
        let topSeparatorView = UIView()
        topSeparatorView.backgroundColor = UIColor.lightGray
        addSubview(topSeparatorView)
    
        topSeparatorView.snp.makeConstraints { (make) in
            make.left.equalTo(snp.left)
            make.bottom.equalTo(snp.bottom).offset(-50)
            make.right.equalTo(snp.right)
            make.height.equalTo(0.5)
        }
        
        let bottomSeparatorView = UIView()
        bottomSeparatorView.backgroundColor = UIColor.lightGray
        addSubview(bottomSeparatorView)
     
        bottomSeparatorView.snp.makeConstraints { (make) in
            make.left.equalTo(snp.left)
            make.bottom.equalTo(snp.bottom)
            make.right.equalTo(snp.right)
            make.height.equalTo(0.5)
        }
        
        
        let stackView = UIStackView(arrangedSubviews: [gridButton,listButton,bookmarkButton])
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        addSubview(stackView)
   
        stackView.snp.makeConstraints { (make) in
            make.left.equalTo(snp.left)
            make.bottom.equalTo(snp.bottom)
            make.right.equalTo(snp.right)
            make.height.equalTo(50)
        }
    
    
    }
}
