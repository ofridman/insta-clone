//
//  PhotoSelectorHeader.swift
//  instaclone
//
//  Created by Axl on 21/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit

class PhotoSelectorHeader: BaseCell {
    let photoImageView : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    override func setupCell() {
        
        super.setupCell()
      
        addSubview(photoImageView)
        
        photoImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
}
