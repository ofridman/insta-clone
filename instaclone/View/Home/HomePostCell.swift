//
//  HomePostCell.swift
//  instaclone
//
//  Created by Axl on 22/01/2018.
//  Copyright © 2018 Axl. All rights reserved.
//

import UIKit
import Firebase

class HomePostCell: BaseCell {
    var post: Post?{
        didSet{
            guard let imageUrl = post?.imageUrl else {return}
            homePostImageView.loadImage(url: imageUrl)
            
            lblUsernameUP.text = post?.user.username
            lblUsername.text = post?.user.username
            
            guard let profileImageUrl = post?.user.profileImageUrl else {return}
            
            profileImageView.loadImage(url: profileImageUrl)
            let creationString = post?.cretaionDate.timeAgoDisplay()
            lblTimeAgo.text = creationString
            lblCaption.text = post?.caption
            guard let userId = Auth.auth().currentUser?.uid else {return}
            guard let postId = post?.postId else {return}
            
            let dbRef = Database.database().reference().child("likes").child(userId).child(postId)
            
            dbRef.observeSingleEvent(of: .value) { (snap) in
                if snap.exists() {
                    self.btnLike.tintColor = UIColor.red
                    self.btnLike.setImage(#imageLiteral(resourceName: "like_selected"), for: .normal)
                    
                   
                }else {
                    self.btnLike.tintColor = UIColor.black
                    self.btnLike.setImage(#imageLiteral(resourceName: "like_unselected"), for: .normal)
                }
            }
            
            
        }
    }
    let profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 40/2
        iv.backgroundColor =  UIColor(white: 0.9, alpha: 0.8)
        return iv
    }()
    let lblUsernameUP: UILabel = {
        let lbl = UILabel()
        lbl.text = "username"
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        
        return lbl
    }()
    
    let homePostImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor =  UIColor(white: 0.9, alpha: 0.8)
        return iv
    }()
    let optionsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("***", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        return btn
    }()
    let btnLike: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let btnComment: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "comment").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let sendMessageButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "send2").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let bookmarkButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "ribbon").withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    let lblUsername: UILabel = {
        let lbl = UILabel()
        lbl.text = "username"
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        
        return lbl
    }()
    let lblTimeAgo: UILabel = {
        let lbl = UILabel()
        lbl.text = "Time Ago"
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor.lightGray
        return lbl
    }()
    let lblCaption: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        //lbl.contentMode = .scaleToFill
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply "
        return lbl
    }()
    
    override func setupCell() {
        setupView()
        btnLike.addTarget(self, action: #selector(handleLike), for: .touchUpInside)
    }
    @objc func handleLike(){
        guard  let postId = self.post?.postId else {return}
        guard let userId = Auth.auth().currentUser?.uid else {return}
        guard let imageUrl = self.post?.imageUrl else {return}
        /// check if like exits
        let dbRef = Database.database().reference().child("likes").child(userId).child(postId)
       // var likeExist = false
        dbRef.observeSingleEvent(of: .value) { (snap) in
            if snap.exists() == false {
                let db = Database.database().reference().child("likes").child(userId)
                let values: [String:Any] = [postId:imageUrl]
                
                db.updateChildValues(values, withCompletionBlock: { (err, _) in
                    
                    if err == nil {
                        self.btnLike.tintColor = UIColor.red
                        self.btnLike.setImage(#imageLiteral(resourceName: "like_selected"), for: .normal)

                    }
                })
                
                
            } else {
                // if like exist remove from db and update button
                dbRef.removeValue()
                self.btnLike.tintColor = UIColor.black
                self.btnLike.setImage(#imageLiteral(resourceName: "like_unselected"), for: .normal)
            }
        }
        
       
    /*     if likeExist == false {
            let values: [String:Any] = [userId:1]

            let likeRef = Database.database().reference().child("posts").child(postId).child("likes")

            likeRef.updateChildValues(values, withCompletionBlock: { (err, _) in
            if let err = err {
                print("error like insert " , err)
                
            }
            })
        } */
 
        
    }
    func setupView(){
        
        
        
        addSubview(profileImageView)
        addSubview(lblUsernameUP)
        addSubview(homePostImageView)
        
        addSubview(btnLike)
        addSubview(btnComment)
        addSubview(lblUsername)
        addSubview(lblTimeAgo)
        addSubview(lblCaption)
        
        profileImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(40)
            make.top.equalTo(snp.top).offset(8)
            make.left.equalTo(snp.left).offset(8)
        }
        lblUsernameUP.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(profileImageView.snp.centerY)
            make.left.equalTo(profileImageView.snp.right).offset(8)
        }
        homePostImageView.snp.makeConstraints { (make) in
            make.left.equalTo(snp.left)
            make.right.equalTo(snp.right)
            make.top.equalTo(profileImageView.snp.bottom).offset(8)
            make.height.equalTo(homePostImageView.snp.width)
        }
        let stackView = UIStackView(arrangedSubviews: [btnLike,btnComment])
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(homePostImageView.snp.bottom).offset(4)
            make.left.equalTo(snp.left).offset(8)
            make.width.equalTo(60)
            make.height.equalTo(40)
            
        }
        lblUsername.snp.makeConstraints { (make) in
            make.top.equalTo(stackView.snp.bottom)
            make.left.equalTo(snp.left).offset(8)
        }
        lblTimeAgo.snp.makeConstraints { (make) in
            
            make.top.equalTo(lblUsername.snp.bottom).offset(4)
            make.left.equalTo(snp.left).offset(8)
        }
        lblCaption.snp.makeConstraints { (make) in
            make.top.equalTo(homePostImageView.snp.bottom).offset(4)
            make.centerY.equalTo(lblUsername.snp.centerY)
            make.left.equalTo(lblUsername.snp.right).offset(8)
            make.right.equalTo(snp.right).offset(-4)
        }

    }
}
